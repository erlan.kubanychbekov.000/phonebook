from phonebook import PhoneBookApp


def main_menu(app):
    """
    Main function for interface output.
    """
    actions = {"1": app.view_entries,
               "2": app.add_entry,
               "3": app.update_entry,
               "4": app.search_entries,
               "5": app.delete_entries,
               "6": "exit",
               }

    while True:
        app.clear_terminal()
        print("\033[1;32mPhonebook Application\033[0m")
        print("\033[1;33m1. View Entries\033[0m")
        print("\033[1;33m2. Add New Entry\033[0m")
        print("\033[1;33m3. Update Entry\033[0m")
        print("\033[1;33m4. Search Entries\033[0m")
        print("\033[1;33m5. Delete Entry\033[0m")
        print("\033[1;33m6. Exit\033[0m")
        choice = input("\033[1;36mSelect an option: \033[0m")
        if actions.get(choice, None) == "exit":
            # Close app
            print("Exiting the application.")
            break
        elif actions.get(choice, None) is None:
            print("\033[1;31mInvalid choice. Please select a valid option.\033[0m")
        else:
            action = actions[choice]
            action()

        input(f"\033[32mPress Enter to continue...\033[0m")


if __name__ == "__main__":
    app = PhoneBookApp('phonebook')

    main_menu(app)

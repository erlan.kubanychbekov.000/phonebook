import os
from typing import List, Dict

from phonebookDB import PhoneBookDB


class PhoneBookApp:
    """
     A console-based Phonebook Application that provides functionalities to manage a phonebook database.

     This application allows users to interactively view, add, update, search, and delete entries in a phonebook.
     The entries include information such as name, surname, patronymic, organization, work phone, and personal phone.

     Attributes:
         phonebook (PhoneBookDB): An instance of the PhoneBookDB class used for database operations.

     Methods:
         clear_terminal(): Clears the console screen.
         main_menu(): Displays the main menu and handles user input to execute corresponding actions.
         view_entries(): Displays all entries in the phonebook.
         add_entry(): Allows the user to add a new entry to the phonebook.
         update_entry(): Allows the user to update an existing entry in the phonebook.
         search_entries(): Allows the user to search for entries based on specified criteria.
         delete_entries(): Allows the user to delete an entry from the phonebook.
         print_entry(entry: dict): Prints a formatted representation of a phonebook entry.

     Usage Example:
         app = PhoneBookApp()
         app.main_menu()
     """

    def __init__(self, db_name):
        self.phonebook: PhoneBookDB = PhoneBookDB(db_name)

    def clear_terminal(self):
        """
        Function to clear the terminal.
        """
        os.system('cls' if os.name == 'nt' else 'clear')

    def view_entries(self):
        """
        Get all entries list.
        """
        self.clear_terminal()
        entries: List[Dict[str, str]] = self.phonebook.get_data()
        for entry in entries:
            self.print_entry(entry)
            print("-" * 50)

    def add_entry(self):
        """
        Add new entry to bd.
        """
        self.clear_terminal()
        print("\033[1;33mCreate Entries\033[0m")
        entry = {
            'surname': input("\033[1;36mEnter surname: \033[0m"),
            'name': input("\033[1;36mEnter name: \033[0m"),
            'patronymic': input("\033[1;36mEnter patronymic: \033[0m"),
            'organization': input("\033[1;36mEnter organization: \033[0m"),
            'work_phone': input("\033[1;36mEnter work phone: \033[0m"),
            'personal_phone': input("\033[1;36mEnter personal phone: \033[0m")
        }
        self.phonebook.create(entry)
        print("\033[1;32mEntry added successfully.\033[0m")

    def update_entry(self):
        """
        Get entry id and update.
        """
        self.clear_terminal()
        print("\033[1;33mUpdate an Entry\033[0m")
        entry_id: int = int(input("\033[1;36mEnter the ID of the entry to update: \033[0m"))
        entry: List[Dict[str, str]] = self.phonebook.get_data(id=entry_id)  # get entry by id
        if entry:
            updated_info: Dict[str, str] = {}
            print("\033[1;36mEnter new values (leave blank to keep current values):\033[0m")
            for key in entry[0]:
                if key != 'id':
                    updated_value: str = input(f"\033[1;36mNew {key.capitalize()} (current: {entry[0][key]}): \033[0m")
                    if updated_value:
                        updated_info[key] = updated_value
            self.phonebook.update(int(entry_id), updated_info)
            print("\033[1;32mEntry updated successfully.\033[0m")
        else:
            print(f"\033[1;31mEntry with ID {entry_id} not found.\033[0m")

    def search_entries(self):
        """
        Search entries by params.
        """
        self.clear_terminal()
        print("\033[1;33mSearch Entries\033[0m")
        search_fields: Dict[str, str] = {
            'surname': input("\033[1;36mEnter surname to search: \033[0m"),
            'name': input("\033[1;36mEnter name to search: \033[0m"),
            'patronymic': input("\033[1;36mEnter patronymic to search: \033[0m"),
            'organization': input("\033[1;36mEnter organization to search: \033[0m"),
            'work_phone': input("\033[1;36mEnter work phone to search: \033[0m"),
            'personal_phone': input("\033[1;36mEnter personal phone to search: \033[0m")
        }
        search_fields = {key: value for key, value in search_fields.items() if value.strip() != ""}
        entries: List[Dict[str, str]] = self.phonebook.get_data(**search_fields)
        if entries:
            for entry in entries:
                self.print_entry(entry)
                print("-" * 50)
        else:
            print("\033[1;31mNo entries found.\033[0m")

    def delete_entries(self):
        """Delete entry by id."""
        self.clear_terminal()
        print("\033[1;33mDelete an Entry\033[0m")
        id_entry = int(input("\033[1;36mEnter ID of the entry to delete: \033[0m"))
        self.phonebook.delete(target_id=id_entry)  # get entry by id.
        print("\033[1;32mEntry deleted.\033[0m")

    def print_entry(self, entry):
        print("-" * 50)
        print(f"\033[1mID: {entry['id']}\033[0m")
        print(f"\033[32mName: {entry['name']} {entry['surname']}\033[0m")
        print(f"\033[33mOrganization: {entry['organization']}\033[0m")
        print(f"\033[34mWork Phone: {entry['work_phone']}\033[0m")
        print(f"\033[35mPersonal Phone: {entry['personal_phone']}\033[0m")

from file_manager import FileManager
from typing import List, Dict


class PhoneBookDB(FileManager):
    """
    A simple text-file-based Phonebook Database for storing and managing phonebook entries.

    This class provides methods for loading, saving, updating, and deleting phonebook entries
    in a text file-based database format. Each entry includes information such as name, surname,
    patronymic, organization, work phone, and personal phone.

    Attributes:
        _db (str): The path to the text file used as the database storage.
        SEPARATOR (str): A separator string used to separate entries in the database.

    Methods:
        __init__(): Initializes the PhoneBookDB instance and loads the initial counter value.
        get_last_counter() -> int: Retrieves the last used ID counter from the database.
        _load() -> List[Dict[str, str]]: Loads entries from the database file and returns a list of dictionaries.
        get_data(**kwargs) -> List[Dict[str, str]]: Retrieves phonebook data, optionally filtered by provided criteria.
        create(data: Dict[str, str]): create a new entry to the database.
        update(target_id: int, data: Dict[str, str]): Updates an existing entry in the database.
        delete(target_id: int): Deletes an entry with the specified ID from the database.

    Usage Example:
        phonebook = PhoneBookDB()
        entries = phonebook.get_data(name="John", organization="Acme Corp")
        print(entries)
    """

    __instance = None

    def __new__(cls, db_name):
        if cls.__instance is None:
            cls.__instance = super().__new__(cls)
            cls._db_path = f"db/{db_name}.txt"
            cls.SEPARATOR = "-" * 50
        return cls.__instance

    def __init__(self, *args):
        pass

    def _counter(self) -> int:
        """
        Getting the last id in the database,
        it is necessary for new records to create an id.
        """
        entries = self.load()
        if entries:
            last_id = int(entries[-1].get('id', 0))
            return last_id
        return 0

    def get_data(self, **kwargs) -> List[Dict[str, str]]:
        """
        Retrieve phonebook data. If kwargs are provided, filter data based on the provided criteria.
        """
        data: List[Dict[str, str]] = self.load()
        if kwargs:
            filtered_data: List[Dict[str, str]] = []
            for entry in data:
                match = all(entry.get(key) == str(value) for key, value in kwargs.items())
                if match:
                    filtered_data.append(entry)
            return filtered_data
        else:
            return data

    def create(self, data: Dict[str, str]):
        """
        create a new phonebook entry to the file.
        """
        new_id = self._counter() + 1

        data['id'] = str(new_id)
        entries = self.load()
        entries.append(data)
        self.save(entries)

    def update(self, target_id: int, data: Dict[str, str]):
        """
        Update an entry with the specified target_id using the given data.
        """
        entries = self.load()

        for entry in entries:
            if entry.get('id') == str(target_id):
                for key, value in data.items():
                    entry[key] = value
                break

        self.save(entries)

    def delete(self, target_id: int):
        """
        Delete a phonebook entry with the specified target_id.
        """
        entries = self.load()
        entries = [entry for entry in entries if entry.get('id') != str(target_id)]

        self.save(entries)

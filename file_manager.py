from typing import List, Dict


class FileManager:
    def __init__(self, db_path):
        self._db_path = db_path
        self.SEPARATOR = "-" * 50

    def load(self)-> List[Dict]:
        entries: List[Dict[str, str]] = []
        entry: Dict[str, str] = {}

        with open(self._db_path, 'r') as file:
            for line in file:
                stripped_line: str = line.strip()
                if stripped_line != self.SEPARATOR:
                    key_value = stripped_line.split(":")
                    if len(key_value) == 2:
                        key, value = key_value
                        entry[key] = value
                else:
                    if entry:
                        entries.append(entry)
                        entry = {}

        return entries

    def save(self, entries: List[Dict[str, str]]):
        updated_content: List[str] = []
        for entry in entries:
            for key, value in entry.items():
                updated_content.append(f"{key}:{value}\n")
            updated_content.append(f'{self.SEPARATOR}\n')

        with open(self._db_path, 'w') as file:
            file.writelines(updated_content)
